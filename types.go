package tigerio

type SecurityType string

const (
	STK  SecurityType = "STK"
	OPT               = "OPT"
	WAR               = "WAR"
	IOPT              = "IOPT"
	CASH              = "CASH"
	FUT               = "FUT"
	FOP               = "FOP"
)

type CurrencyType string

const (
	USD CurrencyType = "USD"
	HKD CurrencyType = "HKD"
	CNH CurrencyType = "CNH"
)

type OrderType string

const (
	MKT    OrderType = "MKT"
	LMT              = "LMT"
	STP              = "STP"
	STPLMT           = "STP_LMT"
	TRAIL            = "TRAIL"
)

type OrderStatusType string

const (
	Invalid       OrderStatusType = "INVALID"
	Initial                       = "INITIAL"
	PendingCancel                 = "PENDINGCANCEL"
	Cancelled                     = "CANCELLED"
	Submitted                     = "SUBMITTED"
	Filled                        = "FILLED"
	Inactive                      = "INACTIVE"
	PendingSubmit                 = "PENDINGSUBMIT"
	Held                          = "HELD"
	Expired                       = "EXPIRED"
)

type AccountType int

const (
	CASHACC AccountType = iota
	MGRNACC
	PMGNACC
)

type AccountStatusType int

const (
	New AccountStatusType = iota
	Funded
	Open
	Pending
	Abandoned
	Rejected
	Closed
	Unknown
)

type SubscriptionType int

const (
	AssetChange SubscriptionType = iota
	OrderChange
	PositionChange
	QuoteChange
	QuoteDepth
	Option
	Future
)

type ActionType string

const (
	BUY  ActionType = "BUY"
	SELL            = "SELL"
)

type TimeInForceType string

const (
	DAY TimeInForceType = "DAY"
	GTC                 = "GTC"
)

type Contract struct {
	ID                    int32        `json:"contract_id" dynamodbav:"contract_id"`
	Symbol                string       `json:"symbol" dynamodbav:"symbol"`
	Currency              CurrencyType `json:"currency" dynamodbav:"currency"`
	SecType               SecurityType `json:"sec_type" dynamodbav:"sec_type"`
	Exchange              string       `json:"exchange" dynamodbav:"exchange"`
	OriginalSymbol        string       `json:"original_symbol" dynamodbav:"original_symbol"`
	LocalSymbol           string       `json:"local_symbol" dynamodbav:"local_symbol"`
	Strike                float32      `json:"strike" dynamodbav:"strike"`
	Multiplier            float32      `json:"multiplier" dynamodbav:"multiplier"`
	Name                  string       `json:"name" dynamodbav:"name"`
	ShortMargin           float32      `json:"short_margin" dynamodbav:"short_margin"`
	ShortFreeRate         float32      `json:"short_free_rate" dynamodbav:"short_free_rate"`
	Shortable             bool         `json:"shortable" dynamodbav:"shortable"`
	LongInitMargin        float32      `json:"long_initial_margin" dynamodbav:"long_initial_margin"`
	LongMaintenanceMargin float32      `json:"long_maintenance_margin" dynamodbav:"long_maintenance_margin"`
	ContractMonth         string       `json:"contract_month" dynamodbav:"contract_month"`
	Identifier            string       `json:"identifier" dynamodbav:"identifier"`

	//PutCall               Unknown      `json:"put_call"`
	//Expiry                Unknown      `json:"expiry"`
}

type RequestOptions struct {
	// Order Get Request Params
	Symbols   string          `url:"symbols,omitempty"`
	Statuses  OrderStatusType `url:"status,omitempty"`
	StartTime string          `url:"start_time,omitempty"`
	EndTime   string          `url:"end_time,omitempty"`

	// Place&Modify&Cancel Order Request Params
	Side        ActionType      `url:"side,omitempty"`
	Size        int32           `url:"size,omitempty"`
	OrderType   OrderType       `url:"order_type,omitempty"`
	LimitPrice  float32         `url:"limit_price,omitempty"`
	OutsideRTH  bool            `url:"outside_rth,omitempty"`
	TimeInForce TimeInForceType `url:"time_in_force,omitempty"`
}
