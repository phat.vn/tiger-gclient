package tigerio

type Order struct {
	Account         string          `json:"account" dynamodbav:account`
	ID              int64           `json:"id" dynamodbav:"id"`
	OrderID         int64           `json:"order_id" dynamodbav:"order_id"`
	ParentID        int64           `json:"parent_id" dynamodbav:"parent_id"`
	OrderTime       int64           `json:"order_time" dynamodbav:"order_time"`
	Reason          string          `json:"reason" dynamodbav:"reason"`
	TradeTime       int64           `json:"trade_time" dynamodbav:"trade_time"`
	Action          ActionType      `json:"action" dynamodbav:"action"`
	Quantity        int32           `json:"quantity" dynamodbav:"quantity"`
	Filled          int32           `json:"filled" dynamodbav:"filled"`
	AvgFillPrice    float32         `json:"avg_fill_price" dynamodbav:"avg_fill_price"`
	Commission      float32         `json:"commission" dynamodbav:"commission"`
	RealizedPNL     float32         `json:"realized_pnl" dynamodbav:"realized_pnl"`
	TrailStopPrice  float32         `json:"trail_stop_price" dynamodbav:"trail_stop_price"`
	LimitPrice      float32         `json:"limit_price" dynamodbav:"limit_price"`
	AuxPrice        float32         `json:"aux_price" dynamodbav:"aux_price"`
	TrailingPercent float32         `json:"trailing_percent" dynamodbav:"trailing_percent"`
	PercentOffset   float32         `json:"percent_offset" dynamodbav:"percent_offset"`
	OrderType       OrderType       `json:"order_type" dynamodbav:"order_type"`
	TimeInForce     TimeInForceType `json:"time_in_force" dynamodbav:"time_in_force"`
	OutsideRTH      bool            `json:"outside_rth" dynamodbav:"outside_rth"`
	OrderLegs       Orders          `json:"order_legs" dynamodbav:"order_legs"`
	Contract        Contract        `json:"contract" dynamodbav:"contract"`
	Status          OrderStatusType `json:"status" dynamodbav:"status"`
	Remaining       int32           `json:"remaining" dynamodbav:"remaining"`
	CreatedAt       int64           `json:"created:_at" dynamodbav:"created_at"`
	UpdatedAt       int64           `json:"updated_at" dynamodbav:"updated_at"`
	AutoTrade       bool            `json:"auto_trade" dynamodbav:"auto_trade"`
	RevisionID      string          `json:"revision_id" dynamodbav:"revision_id"`
}

type Orders []Order

func (ords Orders) ByID() map[int64]Order {
	out := make(map[int64]Order, len(ords))
	for _, ord := range ords {
		out[ord.ID] = ord
	}
	return out
}

func (ords Orders) BySymbol() map[string]Order {
	out := make(map[string]Order, len(ords))
	for _, ord := range ords {
		out[ord.Contract.Symbol] = ord
	}
	return out
}

func (o *Order) OnChangeUpdate(changed Order) {
	o.Quantity = changed.Quantity
	o.Filled = changed.Filled
	o.AvgFillPrice = changed.AvgFillPrice
	o.Status = changed.Status
	o.RealizedPNL = changed.RealizedPNL
	o.OutsideRTH = changed.OutsideRTH
	o.LimitPrice = changed.LimitPrice
	o.TradeTime = changed.TradeTime
	o.OrderTime = changed.OrderTime
}
