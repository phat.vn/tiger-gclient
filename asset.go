package tigerio

type Asset struct {
	Account  string                  `json:"account"`
	Segments map[string]AssetSegment `json:"segments"`
	Summary  AssetSummary            `json:"summary"`
}

type AssetSegment struct {
	Segment               string  `json:"segment"`
	AccuruedCash          float32 `json:"accrued_cash"`
	AccuruedDiv           float32 `json:"accrued_dividend"`
	AvailableFunds        float32 `json:"available_funds"`
	Cash                  float32 `json:"cash"`
	EquityWithLoan        float32 `json:"equity_with_loan"`
	ExcessLiquidity       float32 `json:"excess_liquidity"`
	GrossPositiveValue    float32 `json:"gross_position_value"`
	InitMarginRequirement float32 `json:"initial_margin_requirement"`
	MainMarginRequirement float32 `json:"maintenance_margin_requirement"`
	NetLiquidation        float32 `json:"net_liquidation"`
	REQTEquity            float32 `json:"regt_equity"`
	REQTMargin            float32 `json:"regt_margin"`
	SMA                   float32 `json:"sma"`
	Timestamp             int64   `json:"timestamp"`
}

type AssetSummary struct {
	AccuruedCash          float32      `json:"accrued_cash"`
	AccuruedDiv           float32      `json:"accrued_dividend"`
	AvailableFunds        float32      `json:"available_funds"`
	Cash                  float32      `json:"cash"`
	BuyingPower           float32      `json:"buying_power"`
	Currency              CurrencyType `json:"currency"`
	DayTradesRemaining    int32        `json:"day_trades_remaining"`
	EquityWithLoan        float32      `json:"equity_with_loan"`
	ExcessLiquidity       float32      `json:"excess_liquidity"`
	GrossPositiveValue    float32      `json:"gross_position_value"`
	InitMarginRequirement float32      `json:"initial_margin_requirement"`
	MainMarginRequirement float32      `json:"maintenance_margin_requirement"`
	NetLiquidation        float32      `json:"net_liquidation"`
	RealizedPNL           float32      `json:"realized_pnl"`
	UnrealizedPNL         float32      `json:"unrealized_pnl"`
	REQTEquity            float32      `json:"regt_equity"`
	REQTMargin            float32      `json:"regt_margin"`
	SMA                   float32      `json:"sma"`
	Timestamp             int64        `json:"timestamp"`
	//Cushion Unknown `json:"cushion"`
}

type Assets []Asset

//func (a *Asset) OnChangeUpdate(segment AssetSegment) {
//	if v, ok := a.Segments[segment.Segment]; ok {
//		v.Cash = segment.Cash
//		v.GrossPositiveValue = segment.GrossPositiveValue
//		v.EquityWithLoan = segment.EquityWithLoan
//		v.InitMarginRequirement = segment.InitMarginRequirement
//		v.ExcessLiquidity = segment.ExcessLiquidity
//		v.AvailableFunds = segment.AvailableFunds
//		v.MainMarginRequirement = segment.MainMarginRequirement
//		a[segment.Segment] = v
//	}
//}
