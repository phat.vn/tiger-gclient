package tigerio

import "math"

type Position struct {
	Contract    Contract `json:"contract"`
	Quantity    int32    `json:"quantity"`
	AverageCost float32  `json:"average_cost"`
	MarketPrice float32  `json:"market_price"`
}

type Positions []Position

func (p Position) PnL(lastPrice float32) float32 {
	powFactor := float32(1)
	if lastPrice < p.AverageCost {
		powFactor *= -1
	}
	return powFactor * (float32(math.Pow(float64(lastPrice/p.AverageCost), float64(powFactor))) - float32(1))
}

func (ps Positions) BySymbol() map[string]Position {
	out := make(map[string]Position, len(ps))
	for _, p := range ps {
		out[p.Contract.Symbol] = p
	}
	return out
}

func (p *Position) OnChangeUpdate(changed Position) {
	p.Quantity = changed.Quantity
	p.AverageCost = changed.AverageCost
	p.MarketPrice = changed.MarketPrice
}
